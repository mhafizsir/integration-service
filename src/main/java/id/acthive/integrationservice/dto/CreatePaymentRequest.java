package id.acthive.integrationservice.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreatePaymentRequest {

  private String invoiceNumber;
  private BigDecimal amount;
  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private String billDescription;
  private BigDecimal billTotal;
  private String pgCode;
  private String pgName;
  private String userId;
  private String userName;
  private String userPhone;
  private String userEmail;
  private String productName;
  // OVO
  private String billingName;
}
