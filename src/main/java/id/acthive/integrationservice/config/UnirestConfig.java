package id.acthive.integrationservice.config;

import kong.unirest.Unirest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class UnirestConfig {

  @Bean
  public void metrics() {
    Unirest.config()
        .socketTimeout(180000)
        .connectTimeout(180000)
        .instrumentWith(
            requestSummary -> {
              long startNanos = System.nanoTime();
              return (responseSummary, exception) ->
                  log.info(
                      "path: {} status: {} time: {}",
                      requestSummary.getRawPath(),
                      responseSummary.getStatus(),
                      System.nanoTime() - startNanos);
            });
  }
}
