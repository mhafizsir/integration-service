package id.acthive.integrationservice.external.faspay.model;

import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "faspay_transaction_items")
public class FaspayTransactionItem {

  @Id
  @GeneratedValue
  private Long id;

  @Column(name = "created_at", columnDefinition = "TIMESTAMP WITH TIME ZONE")
  private ZonedDateTime createdAt;

  @Column(name = "modified_at", columnDefinition = "TIMESTAMP WITH TIME ZONE")
  private ZonedDateTime modifiedAt;

  @Column(name = "name", columnDefinition = "TEXT")
  private String name;

  @Column(name = "qty", columnDefinition = "TEXT")
  private String qty;

  @Column(name = "amount", columnDefinition = "TEXT")
  private String amount;

  @Column(name = "tenor", columnDefinition = "TEXT")
  private String tenor;

  @Column(name = "payment_plan", columnDefinition = "TEXT")
  private String paymentPlan;

  @Column(name = "merchant_id", columnDefinition = "TEXT")
  private String merchantId;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "faspay_transaction_id")
  private FaspayTransaction faspayTransaction;
}