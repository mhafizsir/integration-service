package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CancelTransactionRequest {

  private String request;
  @SerializedName("trx_id")
  private String trxId;
  @SerializedName("merchant_id")
  private String merchantId;
  private String merchant;
  @SerializedName("bill_no")
  private String billNo;
  @SerializedName("payment_cancel")
  private String paymentCancel;
  private String signature;
}
