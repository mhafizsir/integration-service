package id.acthive.integrationservice.external.faspay.repository;

import id.acthive.integrationservice.external.faspay.model.FaspayTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaspayTransactionRepository extends JpaRepository<FaspayTransaction, String> {

}
