package id.acthive.integrationservice.external.faspay.repository;

import id.acthive.integrationservice.external.faspay.model.FaspayTransactionItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaspayTransactionItemRepository extends
    JpaRepository<FaspayTransactionItem, Long> {

}
