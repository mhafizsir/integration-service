package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InquiryPaymentStatusResponse {

  @SerializedName("response_code")
  private String responseCode;
  @SerializedName("response_desc")
  private String responseDesc;
  private String request;
  private String signature;
  private String merchant;
  @SerializedName("payment_reff")
  private String paymentReff;
  @SerializedName("payment_date")
  private String paymentDate;
  @SerializedName("payment_status_code")
  private String paymentStatusCode;
  @SerializedName("payment_status_desc")
  private String paymentStatusDesc;
  @SerializedName("trx_id")
  private String trxId;
  @SerializedName("merchant_id")
  private String merchantId;
  @SerializedName("bill_no")
  private String billNo;
}
