package id.acthive.integrationservice.external.faspay.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import id.acthive.integrationservice.dto.CreatePaymentRequest;
import id.acthive.integrationservice.external.faspay.dto.PaymentNotificationRequest;
import id.acthive.integrationservice.external.faspay.service.FaspayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/faspay")
public class FaspayController {

  private final FaspayService faspayService;

  public FaspayController(FaspayService faspayService) {
    this.faspayService = faspayService;
  }

  @PostMapping(value = "/callback", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> callback(HttpEntity<String> requestString, PaymentNotificationRequest request) {

    log.info("Controller Params = {}", requestString);
    return ResponseEntity.ok(faspayService.callback(request));
  }

  @GetMapping("/redirect")
  public ResponseEntity<?> redirect(@RequestParam("trx_id") String trxId,
      @RequestParam("merchant_id") String merchantId,
      @RequestParam("bill_no") String billNo,
      @RequestParam("signature") String signature) {

    return ResponseEntity.ok(faspayService.redirect(trxId, merchantId, billNo, signature));
  }

  @GetMapping("/payment-channel")
  public ResponseEntity<?> getPaymentChannelList() {

    return ResponseEntity.ok(faspayService.inquiryPaymentChannelList());
  }

  @PostMapping(value = "/create-payment", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createPayment(@RequestBody CreatePaymentRequest request) {

    return ResponseEntity.ok(faspayService.postDataTransaction(request));
  }

  @GetMapping("/inquiry")
  public ResponseEntity<?> inquiryTransaction(@RequestParam("trxId") String trxId) {

    return ResponseEntity.ok(faspayService.inquiryTransaction(trxId));
  }
}
