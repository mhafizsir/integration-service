package id.acthive.integrationservice.external.faspay.model;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "faspay_transactions")
public class FaspayTransaction {

  @Id
  @Column(name = "id", columnDefinition = "TEXT")
  private String trxId;

  @Column(name = "redirect_url", columnDefinition = "TEXT")
  private String redirectUrl;

  @Column(name = "created_at", columnDefinition = "TIMESTAMP WITH TIME ZONE")
  private ZonedDateTime createdAt;

  @Column(name = "modified_at", columnDefinition = "TIMESTAMP WITH TIME ZONE")
  private ZonedDateTime modifiedAt;

  @Column(name = "signature", columnDefinition = "TEXT")
  private String signature;

  @Column(name = "merchant_id", columnDefinition = "TEXT")
  private String merchantId;

  @Column(name = "merchant", columnDefinition = "TEXT")
  private String merchant;

  @Column(name = "bill_no", columnDefinition = "TEXT")
  private String billNo;

  @Column(name = "bill_reff", columnDefinition = "TEXT")
  private String billReff;

  @Column(name = "bill_date", columnDefinition = "TEXT")
  private String billDate;

  @Column(name = "bill_expired", columnDefinition = "TEXT")
  private String billExpired;

  @Column(name = "bill_desc", columnDefinition = "TEXT")
  private String billDesc;

  @Column(name = "bill_currency", columnDefinition = "TEXT")
  private String billCurrency;

  @Column(name = "bill_gross", columnDefinition = "TEXT")
  private String billGross;

  @Column(name = "bill_miscfee", columnDefinition = "TEXT")
  private String billMiscfee;

  @Column(name = "bill_total", columnDefinition = "TEXT")
  private String billTotal;

  @Column(name = "cust_no", columnDefinition = "TEXT")
  private String custNo;

  @Column(name = "cust_name", columnDefinition = "TEXT")
  private String custName;

  @Column(name = "payment_channel", columnDefinition = "TEXT")
  private String paymentChannel;

  @Column(name = "pay_type", columnDefinition = "TEXT")
  private String payType;

  @Column(name = "bank_userid", columnDefinition = "TEXT")
  private String bankUserid;

  @Column(name = "msisdn", columnDefinition = "TEXT")
  private String msisdn;

  @Column(name = "email", columnDefinition = "TEXT")
  private String email;

  @Column(name = "terminal", columnDefinition = "TEXT")
  private String terminal;

  @Column(name = "billing_name", columnDefinition = "TEXT")
  private String billingName;

  @Column(name = "billing_lastname", columnDefinition = "TEXT")
  private String billingLastname;

  @Column(name = "billing_address", columnDefinition = "TEXT")
  private String billingAddress;

  @Column(name = "billing_address_city", columnDefinition = "TEXT")
  private String billingAddressCity;

  @Column(name = "billing_address_region", columnDefinition = "TEXT")
  private String billingAddressRegion;

  @Column(name = "billing_address_state", columnDefinition = "TEXT")
  private String billingAddressState;

  @Column(name = "billing_address_poscode", columnDefinition = "TEXT")
  private String billingAddressPoscode;

  @Column(name = "billing_msisdn", columnDefinition = "TEXT")
  private String billingMsisdn;

  @Column(name = "billing_address_country_code", columnDefinition = "TEXT")
  private String billingAddressCountryCode;

  @Column(name = "receiver_name_for_shipping", columnDefinition = "TEXT")
  private String receiverNameForShipping;

  @Column(name = "shipping_lastname", columnDefinition = "TEXT")
  private String shippingLastname;

  @Column(name = "shipping_address", columnDefinition = "TEXT")
  private String shippingAddress;

  @Column(name = "shipping_address_city", columnDefinition = "TEXT")
  private String shippingAddressCity;

  @Column(name = "shipping_address_region", columnDefinition = "TEXT")
  private String shippingAddressRegion;

  @Column(name = "shipping_address_state", columnDefinition = "TEXT")
  private String shippingAddressState;

  @Column(name = "shipping_address_poscode", columnDefinition = "TEXT")
  private String shippingAddressPoscode;

  @Column(name = "shipping_msisdn", columnDefinition = "TEXT")
  private String shippingMsisdn;

  @Column(name = "shipping_address_country_code", columnDefinition = "TEXT")
  private String shippingAddressCountryCode;

  @Column(name = "reserve1", columnDefinition = "TEXT")
  private String reserve1;

  @Column(name = "reserve2", columnDefinition = "TEXT")
  private String reserve2;

  @Column(name = "status", columnDefinition = "TEXT")
  private String status;

  @OneToMany(
      mappedBy = "faspayTransaction",
      cascade = CascadeType.ALL,
      orphanRemoval = true,
      fetch = FetchType.EAGER
  )
  private List<FaspayTransactionItem> faspayTransactionItems = new ArrayList<>();

  public void addFaspayTransactionItem(FaspayTransactionItem faspayTransactionItem) {
    faspayTransactionItems.add(faspayTransactionItem);
    faspayTransactionItem.setFaspayTransaction(this);
  }

  public void removeFaspayTransactionItem(FaspayTransactionItem faspayTransactionItem) {
    faspayTransactionItems.remove(faspayTransactionItem);
    faspayTransactionItem.setFaspayTransaction(null);
  }
}
