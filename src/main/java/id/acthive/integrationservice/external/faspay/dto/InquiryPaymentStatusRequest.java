package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InquiryPaymentStatusRequest {

  private String request;
  private String signature;
  @SerializedName("trx_id")
  private String trxId;
  @SerializedName("merchant_id")
  private String merchantId;
  @SerializedName("bill_no")
  private String billNo;
}
