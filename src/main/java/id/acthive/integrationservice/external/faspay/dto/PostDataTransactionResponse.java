package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDataTransactionResponse {

  private String response;
  private String merchant;

  @SerializedName("trx_id")
  private String trxId;

  @SerializedName("merchant_id")
  private String merchantId;

  @SerializedName("bill_no")
  private String billNo;

  @SerializedName("bill_items")
  private List<ResponseBillItem> billItems;

  @SerializedName("response_code")
  private String responseCode;

  @SerializedName("response_desc")
  private String responseDesc;

  @SerializedName("redirect_url")
  private String redirectUrl;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class ResponseBillItem {

    private String product;
    private String qty;
    private String amount;
    @SerializedName("payment_plan")
    private String paymentPlan;
    @SerializedName("merchant_id")
    private String merchantId;
    private String tenor;
  }
}
