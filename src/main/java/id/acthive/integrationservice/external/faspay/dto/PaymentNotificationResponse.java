package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentNotificationResponse {

  private String response;
  @SerializedName("trx_id")
  private String trxId;
  @SerializedName("merchant_id")
  private String merchantId;
  private String merchant;
  @SerializedName("bill_no")
  private String billNo;
  @SerializedName("response_code")
  private String responseCode;
  @SerializedName("response_desc")
  private String responseDesc;
  @SerializedName("response_date")
  private String responseDate;
}
