package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CancelTransactionReponse {

  private String response;
  private String signature;
  private String merchant;
  @SerializedName("payment_reff")
  private String paymentReff;
  @SerializedName("trx_status_code")
  private String trxStatusCode;
  @SerializedName("trx_status_desc")
  private String trxStatusDesc;
  @SerializedName("trx_id")
  private String trxId;
  @SerializedName("merchant_id")
  private String merchantId;
  @SerializedName("bill_no")
  private String billNo;
  @SerializedName("payment_status_code")
  private String paymentStatusCode;
  @SerializedName("payment_status_desc")
  private String paymentStatusDesc;
  @SerializedName("payment_cancel_date")
  private String paymentCancelDate;
  @SerializedName("payment_cancel")
  private String paymentCancel;
  @SerializedName("response_code")
  private String responseCode;
  @SerializedName("response_desc")
  private String responseDesc;
}
