package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentChannelRequest {

  private String request;

  @SerializedName("merchant_id")
  private String merchantId;

  @SerializedName("merchant")
  private String merchant;

  private String signature;
}
