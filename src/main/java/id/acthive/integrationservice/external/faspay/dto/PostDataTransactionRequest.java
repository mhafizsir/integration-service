package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDataTransactionRequest {

  private String request;
  private String merchant;
  private List<PostDataTransactionItem> item;
  private String reserve1;
  private String reserve2;
  private String signature;

  @SerializedName("merchant_id")
  private String merchantId;

  @SerializedName("bill_no")
  private String billNo;

  @SerializedName("bill_reff")
  private String billReff;

  @SerializedName("bill_date")
  private String billDate;

  @SerializedName("bill_expired")
  private String billExpired;

  @SerializedName("bill_desc")
  private String billDesc;

  @SerializedName("bill_currency")
  private String billCurrency;

  @SerializedName("bill_gross")
  private String billGross;

  @SerializedName("bill_miscfee")
  private String billMiscfee;

  @SerializedName("bill_total")
  private String billTotal;

  @SerializedName("cust_no")
  private String custNo;

  @SerializedName("cust_name")
  private String custName;

  @SerializedName("payment_channel")
  private String paymentChannel;

  @SerializedName("pay_type")
  private String payType;

  @SerializedName("bank_userid")
  private String bankUserid;

  @SerializedName("msisdn")
  private String msisdn;

  @SerializedName("email")
  private String email;

  @SerializedName("terminal")
  private String terminal;

  @SerializedName("billing_name")
  private String billingName;

  @SerializedName("billing_lastname")
  private String billingLastname;

  @SerializedName("billing_address")
  private String billingAddress;

  @SerializedName("billing_address_city")
  private String billingAddressCity;

  @SerializedName("billing_address_region")
  private String billingAddressRegion;

  @SerializedName("billing_address_state")
  private String billingAddressState;

  @SerializedName("billing_address_poscode")
  private String billingAddressPoscode;

  @SerializedName("billing_msisdn")
  private String billingMsisdn;

  @SerializedName("billing_address_country_code")
  private String billingAddressCountryCode;

  @SerializedName("receiver_name_for_shipping")
  private String receiverNameForShipping;

  @SerializedName("shipping_lastname")
  private String shippingLastname;

  @SerializedName("shipping_address")
  private String shippingAddress;

  @SerializedName("shipping_address_city")
  private String shippingAddressCity;

  @SerializedName("shipping_address_region")
  private String shippingAddressRegion;

  @SerializedName("shipping_address_state")
  private String shippingAddressState;

  @SerializedName("shipping_address_poscode")
  private String shippingAddressPoscode;

  @SerializedName("shipping_msisdn")
  private String shippingMsisdn;

  @SerializedName("shipping_address_country_code")
  private String shippingAddressCountryCode;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class PostDataTransactionItem {

    private String product;
    private String qty;
    private String amount;
    private String tenor;

    @SerializedName("payment_plan")
    private String paymentPlan;

    @SerializedName("merchant_id")
    private String merchantId;
  }
}
