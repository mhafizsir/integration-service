package id.acthive.integrationservice.external.faspay.service;

import id.acthive.integrationservice.dto.CreatePaymentRequest;
import id.acthive.integrationservice.external.faspay.dto.InquiryPaymentStatusResponse;
import id.acthive.integrationservice.external.faspay.dto.PaymentChannelDto;
import id.acthive.integrationservice.external.faspay.dto.PaymentNotificationRequest;
import id.acthive.integrationservice.external.faspay.dto.PaymentNotificationResponse;
import id.acthive.integrationservice.external.faspay.dto.PostDataTransactionResponse;

public interface FaspayService {

  PaymentChannelDto inquiryPaymentChannelList();

  PostDataTransactionResponse postDataTransaction(CreatePaymentRequest request);

  void callback(String merchantId, String billNo, String request, String trxId, String billReff, String billTotal, String paymentReff, String paymentDate, String bankUserName,
      String status, String signature);

  String redirect(String trxId, String merchantId, String billNo, String signature);

  InquiryPaymentStatusResponse inquiryTransaction(String trxId);

  PaymentNotificationResponse callback(PaymentNotificationRequest request);
}
