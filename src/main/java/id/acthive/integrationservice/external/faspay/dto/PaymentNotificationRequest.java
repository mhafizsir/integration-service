package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentNotificationRequest {

  @SerializedName("request")
  private String request;
  @SerializedName("trx_id")
  private String trxId;
  @SerializedName("merchant_id")
  private String merchantId;
  @SerializedName("merchant")
  private String merchant;
  @SerializedName("bill_no")
  private String billNo;
  @SerializedName("payment_reff")
  private String paymentReff;
  @SerializedName("payment_date")
  private String paymentDate;
  @SerializedName("payment_status_code")
  private String paymentStatusCode;
  @SerializedName("payment_status_desc")
  private String paymentStatusDesc;
  @SerializedName("bill_total")
  private String billTotal;
  @SerializedName("payment_total")
  private String paymentTotal;
  @SerializedName("payment_channel_uid")
  private String paymentChannelUid;
  @SerializedName("payment_channel")
  private String paymentChannel;
  @SerializedName("signature")
  private String signature;
}

