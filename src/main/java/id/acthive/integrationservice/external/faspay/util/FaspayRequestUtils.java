package id.acthive.integrationservice.external.faspay.util;

import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class FaspayRequestUtils {

  private final Environment environment;

  public FaspayRequestUtils(Environment environment) {
    this.environment = environment;
  }

  public String createSignature() {
    var plainString =
        environment.getProperty("faspay.user.id") + environment.getProperty("faspay.user.password");
    log.info("plainString = {}", plainString);
    var md5 = Hex.encodeHex(DigestUtils.md5(plainString));
    var data = DigestUtils.sha1(new String(md5));
    var sha1 = Hex.encodeHex(data);
    return (new String(sha1));
  }

  public String createSignaturePostData(String invoiceNumber) {
    var plainString =
        environment.getProperty("faspay.user.id")
            + environment.getProperty("faspay.user.password")
            + invoiceNumber;
    log.info("plainString = {}", plainString);
    var md5 = Hex.encodeHex(DigestUtils.md5(plainString));
    var data = DigestUtils.sha1(new String(md5));
    var sha1 = Hex.encodeHex(data);
    return (new String(sha1));
  }
}
