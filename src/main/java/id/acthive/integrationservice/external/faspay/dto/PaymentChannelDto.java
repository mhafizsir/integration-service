package id.acthive.integrationservice.external.faspay.dto;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentChannelDto {

  private String response;

  @SerializedName("merchant_id")
  private String merchantId;

  private String merchant;

  @SerializedName("payment_channel")
  private List<PaymentChannel> paymentChannels;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class PaymentChannel {

    @SerializedName("pg_code")
    private String pgCode;

    @SerializedName("pg_name")
    private String pgName;
  }
}
