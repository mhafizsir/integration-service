package id.acthive.integrationservice.external.faspay.service;

import com.google.gson.Gson;
import id.acthive.integrationservice.dto.CreatePaymentRequest;
import id.acthive.integrationservice.external.faspay.dto.InquiryPaymentStatusRequest;
import id.acthive.integrationservice.external.faspay.dto.InquiryPaymentStatusResponse;
import id.acthive.integrationservice.external.faspay.dto.PaymentChannelDto;
import id.acthive.integrationservice.external.faspay.dto.PaymentChannelRequest;
import id.acthive.integrationservice.external.faspay.dto.PaymentNotificationRequest;
import id.acthive.integrationservice.external.faspay.dto.PaymentNotificationResponse;
import id.acthive.integrationservice.external.faspay.dto.PostDataTransactionRequest;
import id.acthive.integrationservice.external.faspay.dto.PostDataTransactionRequest.PostDataTransactionItem;
import id.acthive.integrationservice.external.faspay.dto.PostDataTransactionResponse;
import id.acthive.integrationservice.external.faspay.model.FaspayTransaction;
import id.acthive.integrationservice.external.faspay.model.FaspayTransactionItem;
import id.acthive.integrationservice.external.faspay.repository.FaspayTransactionRepository;
import id.acthive.integrationservice.external.faspay.util.FaspayRequestUtils;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import kong.unirest.Unirest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FaspayServiceImpl implements FaspayService {

  private final Environment environment;
  private final FaspayRequestUtils faspayRequestUtils;
  private final Gson gson;
  private final FaspayTransactionRepository faspayTransactionRepository;

  public FaspayServiceImpl(
      Environment environment, FaspayRequestUtils faspayRequestUtils, Gson gson,
      FaspayTransactionRepository faspayTransactionRepository) {
    this.environment = environment;
    this.faspayRequestUtils = faspayRequestUtils;
    this.gson = gson;
    this.faspayTransactionRepository = faspayTransactionRepository;
  }

  @Override
  public PaymentChannelDto inquiryPaymentChannelList() {

    var request =
        PaymentChannelRequest.builder()
            .merchant(environment.getProperty("faspay.merchant.name"))
            .merchantId(environment.getProperty("faspay.merchant.id"))
            .request("Request List of Payment Gateway")
            .signature(faspayRequestUtils.createSignature())
            .build();

    log.info("Request Body = {}", gson.toJson(request));
    var responseBody =
        Unirest.post(environment.getProperty("faspay.url.paymentchannel"))
            .body(request)
            .asObject(PaymentChannelDto.class)
            .getBody();
    log.info("Response Body = {}", gson.toJson(responseBody));
    return responseBody;
  }

  @Override
  public PostDataTransactionResponse postDataTransaction(CreatePaymentRequest request) {

    var dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    var now = OffsetDateTime.now().atZoneSimilarLocal(ZoneId.of("Asia/Jakarta"));
    var expiredBillDate = now.plusMinutes(15);
    var itemList = new ArrayList<PostDataTransactionItem>();
    var item = new PostDataTransactionItem();
    item.setProduct(request.getProductName());
    item.setQty("1");
    item.setAmount(request.getAmount().setScale(0, RoundingMode.DOWN).toPlainString());
    item.setPaymentPlan("01");
    item.setMerchantId(environment.getProperty("faspay.merchant.id"));
    item.setTenor("0");
    itemList.add(item);

    var postDataRequest = new PostDataTransactionRequest();
    postDataRequest.setRequest("Post Data Transaction");
    postDataRequest.setMerchantId(environment.getProperty("faspay.merchant.id"));
    postDataRequest.setMerchant(environment.getProperty("faspay.merchant.name"));
    postDataRequest.setBillNo(request.getInvoiceNumber());
    postDataRequest.setBillDate(now.format(dateTimeFormatter));
    postDataRequest.setBillExpired(expiredBillDate.format(dateTimeFormatter));
    postDataRequest.setBillDesc(request.getBillDescription());
    postDataRequest.setBillTotal(
        request.getAmount().setScale(0, RoundingMode.DOWN).toPlainString());
    postDataRequest.setPaymentChannel(request.getPgCode());
    postDataRequest.setPayType("1");
    postDataRequest.setCustNo(request.getUserId());
    postDataRequest.setCustName(request.getUserName());
    postDataRequest.setMsisdn(request.getUserPhone());
    postDataRequest.setTerminal("10");
    postDataRequest.setBillingName(
        request.getPgName().toUpperCase().contains("OVO") ? request.getBillingName() : null);
    postDataRequest.setItem(itemList);
    postDataRequest.setSignature(
        faspayRequestUtils.createSignaturePostData(request.getInvoiceNumber()));

    log.info("Request Body = {}", gson.toJson(postDataRequest));
    var responseBodyString =
        Unirest.post(environment.getProperty("faspay.url.postdatatransaction"))
            .body(postDataRequest)
            .asString()
            .getBody();
    log.info("Response Body = {}", responseBodyString);
    var responseBody = gson.fromJson(responseBodyString,
        PostDataTransactionResponse.class);

    var faspayTransactionItem = new FaspayTransactionItem();
    faspayTransactionItem.setCreatedAt(now);
    faspayTransactionItem.setAmount(responseBody.getBillItems().get(0).getAmount());
    faspayTransactionItem.setMerchantId(responseBody.getBillItems().get(0).getMerchantId());
    faspayTransactionItem.setTenor(responseBody.getBillItems().get(0).getTenor());
    faspayTransactionItem.setQty(responseBody.getBillItems().get(0).getQty());
    faspayTransactionItem.setPaymentPlan(responseBody.getBillItems().get(0).getPaymentPlan());
    faspayTransactionItem.setName(responseBody.getBillItems().get(0).getProduct());

    var faspayTransaction = new FaspayTransaction();

    faspayTransaction.setMerchantId(responseBody.getMerchantId());
    faspayTransaction.setMerchant(responseBody.getMerchant());
    faspayTransaction.setBillNo(responseBody.getBillNo());
    faspayTransaction.setBillDate(postDataRequest.getBillDate());
    faspayTransaction.setBillExpired(postDataRequest.getBillExpired());
    faspayTransaction.setBillDesc(postDataRequest.getBillDesc());
    faspayTransaction.setBillTotal(postDataRequest.getBillTotal());
    faspayTransaction.setPaymentChannel(postDataRequest.getPaymentChannel());
    faspayTransaction.setPayType(postDataRequest.getPayType());
    faspayTransaction.setCustNo(postDataRequest.getCustNo());
    faspayTransaction.setCustName(postDataRequest.getCustName());
    faspayTransaction.setMsisdn(postDataRequest.getMsisdn());
    faspayTransaction.setTerminal(postDataRequest.getTerminal());
    faspayTransaction.setBillingName(postDataRequest.getBillingName());
    faspayTransaction.setSignature(postDataRequest.getSignature());
    faspayTransaction.setTrxId(responseBody.getTrxId());
    faspayTransaction.setRedirectUrl(responseBody.getRedirectUrl());
    faspayTransaction.setStatus("UNPAID");
    faspayTransaction.addFaspayTransactionItem(faspayTransactionItem);
    faspayTransactionRepository.save(faspayTransaction);
    return responseBody;
  }

  @Override
  public void callback(String merchantId, String billNo, String request, String trxId,
      String billReff, String billTotal, String paymentReff, String paymentDate,
      String bankUserName, String status, String signature) {

    log.info("Params = {} - {} - {} - {} - {} - {} - {} - {} - {} - {} - {} ", merchantId, billNo,
        request, trxId, billReff, billTotal, paymentReff, paymentDate, bankUserName, status,
        signature);
    var faspayTransaction = faspayTransactionRepository.getById(trxId);
    faspayTransaction.setStatus("1".equalsIgnoreCase(status) ? "PAID" : "UNPAID");
    faspayTransactionRepository.save(faspayTransaction);
  }

  @Override
  public String redirect(String trxId, String merchantId, String billNo, String signature) {

    return MessageFormat.format("{0}/{1}?trx_id={2}&merchant_id={3}&bill_no={4}",
        environment.getProperty("faspay.url.redirect"), signature, trxId, merchantId, billNo);
  }

  @Override
  public InquiryPaymentStatusResponse inquiryTransaction(String trxId) {

    var faspayTransaction = faspayTransactionRepository.getById(trxId);
    var request = new InquiryPaymentStatusRequest();
    request.setRequest("Inquiry Status Payment");
    request.setTrxId(faspayTransaction.getTrxId());
    request.setMerchantId(faspayTransaction.getMerchantId());
    request.setSignature(faspayTransaction.getSignature());
    request.setBillNo(faspayTransaction.getBillNo());
    log.info("Request = {}", gson.toJson(request));
    var responseString = Unirest.post(environment.getProperty("faspay.url.inquiry"))
        .body(request)
        .asString()
        .getBody();
    log.info("Response String = {}", responseString);
    return gson.fromJson(responseString, InquiryPaymentStatusResponse.class);
  }

  @Override
  public PaymentNotificationResponse callback(PaymentNotificationRequest request) {

    log.info("Service Param = {}", request);
    var paymentNotificationResponse = new PaymentNotificationResponse();
    paymentNotificationResponse.setResponse("Payment Notification");
    paymentNotificationResponse.setTrxId(request.getTrxId());
    paymentNotificationResponse.setMerchantId(request.getMerchantId());
    paymentNotificationResponse.setMerchant(request.getMerchant());
    paymentNotificationResponse.setResponseCode("00");
    paymentNotificationResponse.setResponseDesc("Success");
    paymentNotificationResponse.setResponseDate(
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    return paymentNotificationResponse;
  }
}
