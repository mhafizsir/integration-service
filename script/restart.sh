#!/bin/sh

mkdir -p logs
mkdir -p pids
git fetch
git stash
git checkout -B main
git pull
git stash drop
mvn clean install -DskipTests
kill -9 $(lsof -i :9092 -sTCP:LISTEN |awk 'NR > 1 {print $2}')
java -jar ./target/*.jar >logs/out.txt 2>&1 &